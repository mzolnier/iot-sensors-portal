from django.contrib import admin

from IoTsensors.models import Sensor, Project, Data

admin.site.register(Sensor)
admin.site.register(Project)
admin.site.register(Data)