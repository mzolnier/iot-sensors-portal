from django.contrib.auth.models import User
from django.db import models
#from phonenumber_field.modelfields import PhoneNumberField
#create sensor class X,Y,Z


#komentarze, przykład relaji jeden do wielu one2many


class Sensor(models.Model):
    mac_id = models.CharField(max_length=100, null=False)
    sensor_type = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=256, default='brak opisu', null=False)
    alarm_min_x = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    alarm_max_x = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    alarm_min_y = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    alarm_max_y = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    alarm_min_z = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    alarm_max_z = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    scale_factor_X = models.DecimalField(max_digits=10, decimal_places=5, default=1.0, null=False)
    offset_X = models.DecimalField(max_digits=10, decimal_places=5, default=0.0, null=False)
    scale_factor_Y = models.DecimalField(max_digits=10, decimal_places=5, default=1.0, null=False)
    offset_Y = models.DecimalField(max_digits=10, decimal_places=5, default=0.0, null=False)
    scale_factor_Z = models.DecimalField(max_digits=10, decimal_places=5, default=1.0, null=False)
    offset_Z = models.DecimalField(max_digits=10, decimal_places=5, default=0.0, null=False)
    class Meta:
        verbose_name = "Czujnik"
        verbose_name_plural = "Czujniki"

class Project(models.Model):
    name = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=256, null=False)
    #m2m relation
    users = models.ManyToManyField(User)
    sensor = models.ManyToManyField(Sensor)
    class Meta:
        verbose_name = "Projekt"
        verbose_name_plural = "Projekty"

class Data(models.Model):
    sensor = models.ForeignKey(Sensor,on_delete=models.CASCADE, db_index=True)
    timestamp = models.DateTimeField(blank=True, null=True, db_index=True)
    x_value = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    y_value = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    z_value = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    frequency = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    def __str__(self):
        return f"{self.sensor} {self.timestamp} {self.x_value} {self.y_value} {self.z_value} {self.frequency}"
    class Meta:
        verbose_name = "dana"
        verbose_name_plural = "dane"
