from django.apps import AppConfig


class IotsensorsConfig(AppConfig):
    name = 'IoTsensors'
